import React, { Component } from 'react';
import { Root, CardContentConatiner, CardContentText } from './FeedCard.style';

import FeedCardHeader from './FeedCardHeaders';
import FeedCardBottom from './FeedCardBottom';

const text = 'This is a sample text and trying to be so cool to post this!';
class FeedCard extends Component {
  render() {
    return (
      <Root>
        <FeedCardHeader />
        <CardContentConatiner>
          <CardContentText>
            {text}
          </CardContentText>
        </CardContentConatiner>
        <FeedCardBottom />
      </Root>
    )
  }
}

export default FeedCard;
