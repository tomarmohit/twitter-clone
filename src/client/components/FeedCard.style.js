import style from 'styled-components/native';

export const Root = style.View`
minHeight: 180;
  backgroundColor: ${props => props.theme.WHITE};
  width: 100%;
  padding: 7px;
  shadowColor: ${props => props.theme.SECONDARY};
  shadowOffset: 0px 2px;
  shadowRadius: 2;
  shadowOpacity: 0.1;
  marginVertical: 5;      
`;

export const CardContentConatiner = style.View`
flex:1;
padding: 10px 20px 10px 0px
`;

export const CardContentText = style.Text`
fontSize:14;
textAlign:left;
fontWeight:500;
color:${props => props.theme.SECONDARY}
`
