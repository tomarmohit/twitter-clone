import React from 'react';
import {
  Root, AvatarContainer,
  MetaContainer,
  MetaTopContainer,
  MetaBottomContainer,
  Avatar,
  MetaText,
  MetaFullName
} from './FeedCardHeaders.style';

import { avatar } from '../config/colors';

const username = 'palashg7563';
const firstName = 'Palash';
const lastName = 'Gupta';
const createdAt = '1 day ago';


function FeedCardHeader() {
  return (
    <Root>
      <AvatarContainer>
        <Avatar source={{ uri: avatar }} />
      </AvatarContainer>
      <MetaContainer>
        <MetaTopContainer>
          <MetaFullName>
            {firstName} {lastName}
          </MetaFullName>
          <MetaText style={{ marginLeft: 5 }}>
            @{username}
          </MetaText>
        </MetaTopContainer>
        <MetaBottomContainer>
          <MetaText>
            {createdAt}
          </MetaText>
        </MetaBottomContainer>
      </MetaContainer>
    </Root>
  )
}

export default FeedCardHeader;