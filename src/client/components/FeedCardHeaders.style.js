import style from 'styled-components/native';

const AVATAR_SIZE = 40;
const AVATAR_RADUIS = AVATAR_SIZE / 2;


export const Root = style.View`
height: 50;
flexDirection: row;
alignItems: center;
`;

export const AvatarContainer = style.View`
flex:0.2;
justifyContent:center;
alignSelf:stretch;
`;

export const Avatar = style.Image`
height:${AVATAR_SIZE};
width:${AVATAR_SIZE};
borderRadius:${AVATAR_RADUIS}
`;

export const MetaTopContainer = style.View`
flex:1;
alignSelf:stretch;
alignItems:center;
justifyContent:flex-start;
flexDirection:row;
`;

export const MetaBottomContainer = style.View`
flex:0.8;
alignItems:flex-start;
justifyContent:center;
alignSelf:stretch;
`;

export const MetaContainer = style.View`
flex:1;
alignSelf:stretch;
`;

export const MetaText = style.Text`
fontSize:14;
fontWeight:600;
color:${props => props.theme.LIGHT_GRAY}
`;

export const MetaFullName = style.Text`
fontSize:14;
fontWeight:600;
color:${props => props.theme.SECONDARY}
`;