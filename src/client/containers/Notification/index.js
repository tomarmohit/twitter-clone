import React, { Component } from 'react';

import { Root, Text } from './style';

class Notification extends Component {
  render() {
    return (
      <Root>
        <Text>
          Notification
        </Text>
      </Root>
    )
  }
}

export default Notification;