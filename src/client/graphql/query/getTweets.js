import { gql } from 'react-apollo';

export default gql`
  {
    getTweets{
    _id
    text
    favoriteCount
    user{
      username
      avatar
      firstName
      lastName
    }
  }
  }
`;