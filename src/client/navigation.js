import React, { Component } from "react";

import { StackNavigator, TabNavigator } from "react-navigation";
import addNavigationHelpers from "react-navigation/src/addNavigationHelpers";

import { connect } from "react-redux";

import { FontAwesome } from "@expo/vector-icons";

import HomeScreen from "./containers/LandingPage/index";
import ExploreScreen from "./containers/ExploreScreen/index";
import NotificationScreen from "./containers/Notification/index";
import ProfileScreen from "./containers/Profile/index";

import { colors } from "./config/colors";

const TAB_ICON_SIZE = 20;

const Tabs = TabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: () => ({
        headerTitle: "Home",
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="home" />
        )
      }),
      headerRight: () => (
        <FontAwesome
          size={TAB_ICON_SIZE}
          color={colors.SECONDARY}
          name="home"
        />
      )
    },
    Explore: {
      screen: ExploreScreen,
      navigationOptions: () => ({
        headerTitle: "Explore",
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="search" />
        )
      })
    },
    Notification: {
      screen: NotificationScreen,
      navigationOptions: () => ({
        headerTitle: "Notification",
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="bell" />
        )
      })
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: () => ({
        headerTitle: "Profile",
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="user" />
        )
      })
    }
  },
  {
    lazy: true,
    tabBarPosition: "bottom",
    swipeEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      activeTintColor: colors.PRIMARY,
      inactiveTintColor: colors.LIGHT_GRAY,
      style: {
        backgroundColor: colors.WHITE,
        height: 50,
        paddingVertical: 5
      },
      swipeEnabled: false
    }
  }
);

const AppMainNav = StackNavigator(
  {
    Home: {
      screen: Tabs
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F1F6FA"
    },
    navigationOptions: () => ({
      headerStyle: {
        backgroundColor: colors.WHITE
      },
      headerTitleStyle: {
        fontWeight: "bold",
        color: colors.SECONDARY
      }
    })
  }
);

class AppNavigator extends Component {
  render() {
    const nav = addNavigationHelpers({
      dispatch: this.props.dispatch,
      state: this.props.nav
    });
    return <AppMainNav navigation={nav} />;
  }
}

export default connect(state => ({
  nav: state.nav
}))(AppNavigator);

export const router = AppMainNav.router;
