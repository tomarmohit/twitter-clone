import TweetResolver from './tweet';
import UserResolver from './user';
import Users from '../../mongodb/models/Users';

import GraphqlDate from 'graphql-date';

export default {
  Date: GraphqlDate,
  Tweet: {
    user: ({ user }) => Users.findById(user._id)
  },
  Query: {
    getTweet: TweetResolver.getTweet,
    getTweets: TweetResolver.getTweets,
    getUserTweet: TweetResolver.getUserTweet,
    me: UserResolver.me,
  },
  Mutation: {
    createTweet: TweetResolver.createTweet,
    updateTweet: TweetResolver.updateTweet,
    deleteTweet: TweetResolver.deleteTweet,
    signup: UserResolver.signup,
    login: UserResolver.login
  }
}