import User from '../../mongodb/models/Users';
import { requireAuth } from '../../services/auth';

export default {
  signup: async (_, { fullName, ...rest }) => {
    try {
      const [firstName, ...lastName] = fullName.split(' ');
      const user = await User.create({ firstName, lastName, ...rest });
      return { token: user.createToken() };
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  login: async (_, { email, password }) => {
    try {
      const user = await User.findOne({ email });

      if (!user) {
        throw Error(`user doesn't exist`);
        console.log(`user doesn't exist`);
      }

      if (!user.authentication(password)) {
        throw new Error(`Password doesn't match`);
        console.log(`Password doesn't match`);
      }

      return { token: user.createToken() };
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  me: async (_, args, { user }) => {
    try {
      return requireAuth(user);
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}