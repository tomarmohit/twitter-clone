import mongoose from 'mongoose';
import constant from './constant';

mongoose.Promise = global.Promise;

//debug mode true
mongoose.set('debug', true);

try {
  mongoose.connect(constant.DB_URL, { useNewUrlParser: true })
} catch (error) {
  mongoose.createConnection(constant.DB_URL, { useNewUrlParser: true });
}

mongoose.connection
  .once('open', () => console.log('MongoDb is running'))
  .on('error', (err) => {
    console.log(err);
    throw err;
  })
