import mongoose, { Schema } from 'mongoose';
import { hashSync, compareSync, genSaltSync } from 'bcrypt-nodejs';

import sign from 'jsonwebtoken/sign';

import constant from '../config/constant';

const UserSchema = new Schema({
  username: {
    type: String,
    // unique: true
  },
  firstName: String,
  lastName: String,
  avatar: String,
  password: String,
  email: String
}, { timestamps: true });

UserSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = this._hashpassword(this.password);
    return next();
  }
  return next();
})

UserSchema.methods = {
  _hashpassword(password) {
    const no = genSaltSync(12);
    return hashSync(password, no);
  },
  authentication(password) {
    return compareSync(password, this.password)
  },
  createToken() {
    return sign(
      {
        _id: this._id
      },
      constant.SECRET_KEY
    )
  }
}

export default mongoose.model('User', UserSchema)